package cache.sample.portlet.portlet;

import java.io.IOException;
import java.util.HashMap;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.cache.MultiVMPool;
import com.liferay.portal.kernel.cache.PortalCache;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.HttpUtil;

import cache.sample.portlet.constants.CacheSamplePortletKeys;

/**
 * @author Sébastien Le Marchand
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + CacheSamplePortletKeys.CacheSample,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class CacheSamplePortlet extends MVCPortlet {
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		
		PortalCache<String, HashMap> cache = (PortalCache<String, HashMap>) _multiVMPool.getPortalCache("CacheSamplePortlet");
		
		String cacheKey = "repo";
		
		HashMap repo = cache.get(cacheKey);

		if(repo == null) {
			
			String jsonString = HttpUtil.URLtoString("https://api.github.com/repos/liferay/liferay-portal");
		
			repo = JSONFactoryUtil.looseDeserialize(jsonString, HashMap.class);
			
			cache.put(cacheKey, repo);
		}
		
		renderRequest.setAttribute("repo", repo);
		
		super.doView(renderRequest, renderResponse);
	}

	@Reference(unbind = "-")
	protected void setMultiVMPool(MultiVMPool multiVMPool) {
		_multiVMPool = multiVMPool;
	}
	
	private MultiVMPool _multiVMPool;
}